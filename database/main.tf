resource "random_id" "sqldb" {
  byte_length = 4
  prefix      = "sql-s029115-"
}


module "postgresql" {
  source               = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version              = "8.0.0"
  name                 = "${random_id.sqldb.hex}"
  random_instance_name = true
  database_version     = var.database_version #"POSTGRES_10"
  project_id           = var.project_id
  zone                 = var.zones[0]
  region               = var.region
  tier                 = var.tier

  user_name            = "postgres"
  user_password        = var.sql_user_password

  deletion_protection = false

  ip_configuration = {
    ipv4_enabled        = true
    private_network     = null
    require_ssl         = false
    authorized_networks = var.authorized_networks
  }

  db_name      = "yelbdatabase"
  db_charset   = "UTF8"
  db_collation = "en_US.UTF8"

}


output "instance_ip_address" {
  value       = module.postgresql.public_ip_address
  description = "The IPv4 address assigned for the master instance"
}

output "instance_connection_name" {
  value       = module.postgresql.instance_connection_name
  description = "The connection name of the master instance to be used in connection strings"
}

output "psql_user_pass" {
  value       = module.postgresql.generated_user_password
  description = "The password for the default user. If not set, a random one will be generated and available in the generated_user_password output variable."
}
variable "tier" {
    default = "db-f1-micro"
}

variable "database_version" {
    default = "POSTGRES_10"
}

variable "project_id" {}

variable "region" {
  type        = string
  description = "The region to create the cluster."
}

variable "zones" {
  type        = list(string)
  description = "The zones to create the cluster."
}

variable "authorized_networks" {
  default = [{
    name  = "all"
    value = "0.0.0.0/0"
  }]
  type        = list(map(string))
  description = "List of mapped public networks authorized to access to the instances. "
}

variable "sql_user_password" {}

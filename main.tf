
data "google_client_config" "default" {}
data "google_client_openid_userinfo" "terraform_user" {}


######################################
#
# Create network
#
######################################

module "network" {
  source = "./network"

  project_id              = var.project_id
  network                 = local.network
  subnetwork              = local.subnetwork
  region                  = var.region
}

######################################
#
# Create Gitlab Runner VM instance
#
######################################

module "gitlab-runner" {
  source = "./runner"

  project_id                        = var.project_id
  region                            = var.region
  zones                             = var.zones
  network                           = local.network
  subnetwork                        = local.subnetwork
  gitlab_runner_registration_token  = var.gitlab_runner_registration_token

  depends_on = [module.network]
}

######################################
#
# Create Kubernetes Cluster
#
######################################

module "k8s" {
  source = "./gke"

  credentials               = var.credentials
  project_id                = var.project_id
  region                    = var.region
  zones                     = var.zones
  cluster_name              = local.cluster_name
  network                   = local.network
  subnetwork                = local.subnetwork
  node_pool_name            = local.node_pool_name

  depends_on = [module.network]

}

######################################
#
# Create Kubernetes Ingress-Controller
#
######################################

module "ingress-controller" {
  source = "./gke/nginx_ingress"

  depends_on = [module.k8s]
}

######################################
#
# Create Kubernetes Cert-Manager
#
######################################

module "cert-manager" {
  source = "./gke/cert_manager"

  depends_on = [module.ingress-controller]
}

######################################
#
# Create Kubernetes Redis
#
######################################

module "redis" {
  source = "./gke/redis"

  depends_on = [module.ingress-controller]
}

######################################
#
# Create Database VM instance
#
######################################

module "sqldb" {
  source = "./database"

  project_id                        = var.project_id
  region                            = var.region
  zones                             = var.zones
  sql_user_password                 = var.sql_user_password

  depends_on = [module.redis]
}
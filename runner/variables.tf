variable "project_id" {
  description = "The project_id to deploy the example instance into."
}

variable "region" {
  description = "The region to deploy to"
}

variable "network" {
  description = "The network name to deploy to"
}

variable "subnetwork" {
  description = "The network name to deploy to"
}

variable "gitlab_runner_registration_token" {
  description = "gitlab_runner_registration_token"
  type = string
}

variable "zones" {
  type        = list(string)
  description = "The zones to create the cluster."
}

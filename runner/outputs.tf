output "nat_ip" {
  description = "Public IP address of the example compute instance."
  value       = google_compute_instance.runner.network_interface[0].access_config[0].nat_ip
}
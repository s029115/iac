data "google_compute_image" "os" {
  project = "ubuntu-os-cloud"
  family  = "ubuntu-1804-lts"
}

data "template_file" "startup_script" {
  template = file("${path.module}/startup-script.tpl")

  vars = {
    gitlab_runner_registration_token = var.gitlab_runner_registration_token
    project_id = var.project_id
  }
}

resource "google_compute_instance" "runner" {
  name           = "gitlab-runner"
  description    = "VM for Gitlab runner"
  machine_type   = "g1-small"
  can_ip_forward = false
  zone           = var.zones[0]

  metadata = {
    block-project-ssh-keys = true
  }

  metadata_startup_script = data.template_file.startup_script.rendered

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
    preemptible         = false
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image = data.google_compute_image.os.self_link
      type  = "pd-standard"
    }
  }

  network_interface {
    network = var.network
    subnetwork = var.subnetwork

    access_config {
      // Ephemeral IP
    }
  }
}
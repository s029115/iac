locals {
  network        = "${var.project_id}-network"
  subnetwork     = "${var.project_id}-subnetwork"
  cluster_name   = "${var.project_id}-cluster"
  node_pool_name = "${var.project_id}-pool"
}

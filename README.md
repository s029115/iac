# Финальный проект студента s029115 

В проекте используется два репозитория:
   - [IaC](https://gitlab.com/s029115/iac)
   - [Graduation Work](https://gitlab.com/s029115/graduation_work)

Приложение доступоно по адресу https://s029115.ru

## Инфраструктура 

### Папки (terraform модули)
```
database - Cloud SQL
gke - Kubernetes cluster
 - cert_manager - Cert менеджер
 - nginx_ingress - Ingress контроллер
 - redis - Redis сервер
network - Сеть
runner - Раннер для CI
```

### Первоначальная настройка GCP

Для первого запуска terraform плана, необходимо на локальной машине установить утилиту **gcloud**, **kubectl**, **terraform**.

#### Настраиваем GCP
**Создаем проект**
```
gcloud projects create fps029115 --name="Final Slurm Project s029115" --set-as-default --verbosity=debug
```
**Создаем сервисный аккаунт и выдаем роли**
```
gcloud iam service-accounts create s029115 --display-name="Service Account for Final Slurm Project"

gcloud projects add-iam-policy-binding fps029115 --member serviceAccount:s029115@fps029115.iam.gserviceaccount.com --role roles/compute.admin
gcloud projects add-iam-policy-binding fps029115 --member serviceAccount:s029115@fps029115.iam.gserviceaccount.com --role roles/iam.serviceAccountAdmin
gcloud projects add-iam-policy-binding fps029115 --member serviceAccount:s029115@fps029115.iam.gserviceaccount.com --role roles/resourcemanager.projectIamAdmin
gcloud projects add-iam-policy-binding fps029115 --member serviceAccount:s029115@fps029115.iam.gserviceaccount.com --role roles/container.admin
gcloud projects add-iam-policy-binding fps029115 --member serviceAccount:s029115@fps029115.iam.gserviceaccount.com --role roles/logging.admin
```
**Получаем файл с credentials**
```
gcloud iam service-accounts keys create terraform-gcp.json --iam-account=s029115@fps029115.iam.gserviceaccount.com
gcloud auth activate-service-account --key-file=terraform-gcp.json
```
**Включаем API**
```
gcloud services enable compute.googleapis.com container.googleapis.com sql-component.googleapis.com sqladmin.googleapis.com servicenetworking.googleapis.com cloudresourcemanager.googleapis.com dns.googleapis.com
```
**Создаем хранилище состояния terraform в GCP**
```
gsutil mb -p fps029115 -c regional -l EUROPE-WEST3 gs://fps029115/
gsutil versioning set on gs://fps029115/
gsutil iam ch serviceAccount:s029115@fps029115.iam.gserviceaccount.com:legacyBucketWriter gs://fps029115/
```

#### Первый запуск
При первом запуске, переменные для terraform берем из файла ***variables.auto.tfvars*** 
Так же, переменные из этого файла переносим в переменные Gitlab

***Settings > CI/CD > Variables (expand)***
```
PROJECT_ID - идентификатор проекта в GCP
RUNNER_TOKEN - регистрационный токен из Settings > CI/CD > Runners (expand) > Specific runners > registration token
SERVICEACCOUNT - сервисный аккаунт GCP 
SQL_USER_PASSWORD - сложный пароль для пользователя SQL
```

Сервисный аккаунт GCP берем из credentials от GCP выполнив в папке с проектом ***cat ./cred/terraform-gcp.json | base64 -w 0 ; echo***

После первого запуска, раннер должен зарегистрироваться и пайплайны могут работать.
Для проекта **Graduation Work** необходимо включить использование нашего раннера `Enable for this project`.

Так же, необходимо внести A запись в DNS своего домена. 
Получить её можно, посмотрев IP адрес LoadBalancer'a командой ***kubectl get service -n ingress-nginx ingress-nginx-controller***
```
NAME                       TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)                      AGE
ingress-nginx-controller   LoadBalancer   10.30.188.22   34.159.76.213   80:31128/TCP,443:30952/TCP   29m
```

Необходимо создать секрет с ключом от сервисного акаунта терраформа для sql proxy
```
kubectl create secret generic cloudsql-instance-credentials --from-file=terraform.json=cred/terraform-gcp.json -n production
```


## Приложение

#### Точка входа
Добавляем точку входа в проекте **Graduation Work** в переменную ***K8S_API_URL***
```kubectl config view -ojsonpath='{.clusters[0].cluster.server}' ; echo```

#### Deploy token
В **Settings** > **Repository** >  **Deploy tokens (expand)** в проекте ***Graduation Work*** 

В поле Name вводим ***k8s-pull-token*** и ставим галочку ***read_registry***, все остальные поля оставляем пустыми.
Нажимаем **Create deploy token**.

**НЕ ЗАКРЫВАЕМ ОКНО БРАУЗЕРА!**

#### Cluster Role Binding
Создаем ***image pull secret*** - для того, чтобы наш кластер Kubernetes мог получать образы из registry gitlab'а.

```
kubectl create ns production

kubectl create serviceaccount -n production gitlab

kubectl create secret docker-registry --docker-username=<имя_пользователя> \
  --docker-password=<ваш_пароль> \
  --docker-server=registry.gitlab.com -n production gitlab-registry

kubectl create clusterrolebinding --clusterrole=cluster-admin --serviceaccount=production:gitlab ci -n production
```
Получаем токен и заносим в переменную **K8S_CI_TOKEN**
```
kubectl get secret -n production $( kubectl get serviceaccount -n production gitlab -o jsonpath='{.secrets[].name}' ) -o jsonpath='{.data.token}' | base64 -d ; echo
```

#### База данных для приложения

##### Узнать IP для коннекта к БД.
```
gcloud sql instances list | awk {'print $5'} | grep -v 'PRIMARY_ADDRESS'
```

База данных для приложения была создана на этапе формирования инфраструктуры. Нам осталось подключиться к ней и внести первичные данные. 
Сделать это можно, например, с нашего **gitlab-runner**, пароль берём из нашей переменной **SQL_USER_PASSWORD**

```
gcloud compute ssh gitlab-runner

apt install postgresql-client-common postgresql-client -y

export PGPASSWORD="<наш пароль из SQL_USER_PASSWORD>"

psql -v ON_ERROR_STOP=1 -h <наш IP> --username postgres <<-EOSQL
CREATE TABLE restaurants (name char(30), count integer, PRIMARY KEY (name));
INSERT INTO restaurants (name, count) VALUES ('outback', 0);
INSERT INTO restaurants (name, count) VALUES ('bucadibeppo', 0);
INSERT INTO restaurants (name, count) VALUES ('chipotle', 0);
INSERT INTO restaurants (name, count) VALUES ('ihop', 0);
EOSQL
```

##### Создаем секрет для БД
```
kubectl create secret generic db \
  --from-literal=username=postgres \
  --from-literal=password=<наш пароль из SQL_USER_PASSWORD> \
  --from-literal=connectionname=$( gcloud sql instances describe $( gcloud sql instances list | awk {'print $1'} | grep -v 'NAME' ) | grep connectionName | awk {'print $2'} ) \
  -n production
```


![monitoring.png](./monitoring.png)

![logging.png](./logging.png)


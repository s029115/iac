resource "helm_release" "redis" {    
    name = "redis-cluster"
    repository = "https://charts.bitnami.com/bitnami"
    chart = "redis"
    version = "15.6.10"
    
    values = [
      "${file("${path.module}/resources/redis_values.yaml")}"
    ]
}
 


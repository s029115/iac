# google_client_config and kubernetes provider must be explicitly specified like the following.
data "google_client_config" "default" {}
data "google_client_openid_userinfo" "terraform_user" {}

module "gke_auth" {
  source = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  depends_on   = [module.gke]
  project_id   = var.project_id
  location     = module.gke.location
  cluster_name = module.gke.name
}

# ---------------------------------------------------------------------------------------------------------------------
# CONFIGURE KUBECTL AND RBAC ROLE PERMISSIONS
# ---------------------------------------------------------------------------------------------------------------------

# configure kubectl with the credentials of the GKE cluster

resource "null_resource" "configure_kubectl" {
  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${var.cluster_name} --region ${var.region}"
  }

  provisioner "local-exec" {
    command = "env"

    # Use environment variables to allow custom kubectl config paths
    environment = {
      KUBECONFIG = "~/.kube/config"
    }
  }

  depends_on = [module.gke]
}

resource "kubernetes_cluster_role_binding" "user" {
  metadata {
    name = "admin-user"
  }

  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    kind      = "User"
    name      = data.google_client_openid_userinfo.terraform_user.email
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    kind      = "Group"
    name      = "system:masters"
    api_group = "rbac.authorization.k8s.io"
  }
}

module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  project_id                 = var.project_id
  name                       = var.cluster_name
  regional                   = true
  region                     = var.region
  zones                      = var.zones
  network                    = var.network
  subnetwork                 = var.subnetwork
  ip_range_pods              = var.ip_range_pods_name
  ip_range_services          = var.ip_range_services_name
  http_load_balancing        = true
  horizontal_pod_autoscaling = false

  node_pools = [
    {
      name                      = var.node_pool_name
      machine_type              = var.machine_type
      node_locations            = var.zones[0]
      min_count                 = var.min_count
      max_count                 = var.max_count
      local_ssd_count           = 0
      disk_size_gb              = var.disk_size_gb
      disk_type                 = "pd-standard"
      image_type                = "COS"
      auto_repair               = true
      auto_upgrade              = true
      service_account           = var.service_account
      preemptible               = false
      initial_node_count        = var.initial_node_count
    },
 ]

  node_pools_oauth_scopes = {
    all = []

    "${var.node_pool_name}" = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {}

    "${var.node_pool_name}" = {
      "${var.node_pool_name}" = true
    }
  }

  node_pools_metadata = {
    all = {}

    "${var.node_pool_name}" = {
      node-pool-metadata-custom-value = "${var.node_pool_name}"
    }
  }

  node_pools_taints = {
    all = []

    "${var.node_pool_name}" = [
      {
        key    = "${var.node_pool_name}"
        value  = true
        effect = "PREFER_NO_SCHEDULE"
      },
    ]
  }

  node_pools_tags = {
    all = []

    "${var.node_pool_name}" = [
      "${var.node_pool_name}",
    ]
  }

}


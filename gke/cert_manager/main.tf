resource helm_release cert-manager {
  name       = "cert-manager"
  namespace  = "cert-manager"

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.6.1"

  force_update     = false
  create_namespace = true

  set {
    name  = "installCRDs"
    value = true
  }

  set {
    name  = "global.podSecurityPolicy.enabled"
    value = true
  }

  set {
    name  = "global.podSecurityPolicy.useAppArmor"
    value = true
  }

  set {
    name  = "ingressShim.defaultIssuerName"
    value = "letsencrypt"
  }

  set {
    name  = "ingressShim.defaultIssuerKind"
    value = "ClusterIssuer"
  }


}

# --set ingressShim.defaultIssuerName=letsencrypt \
# --set ingressShim.defaultIssuerKind=ClusterIssuer \

/*
resource helm_release issuer {

  depends_on = [ helm_release.cert-manager ]

  name      = "certs"
  namespace = cert-manager
  chart     = "${path.module}/certs"

  force_update    = true
  cleanup_on_fail = true
  recreate_pods   = false
  reset_values    = false

  create_namespace = true

  values = [local.helm_chart_values]

}

*/
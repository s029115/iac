/* Base VARS */
variable "credentials" {
  type        = string
  description = "Location of the credential keyfile."
}

variable "project_id" {
  type        = string
  description = "The project ID to create the cluster."
}

variable "region" {
  type        = string
  description = "The region to create the cluster."
}

variable "zones" {
  type        = list(string)
  description = "The zones to create the cluster."
}

/* Cluster VARS */

variable "machine_type" {
  type          = string
#   default       = "g1-small"
  default = "e2-standard-2"
}

variable "min_count" {
  type          = number
  default       = 3
}

variable "max_count" {
  type          = number
  default       = 5
}

variable "disk_size_gb" {
  type        = number
  default = 15
}

variable "service_account" {
  type        = string
  default = "s029115@fps029115.iam.gserviceaccount.com"
}

variable "initial_node_count" {
  type        = number
  default = 3
}

variable "ip_range_pods_name" {
  description = "The secondary ip range to use for pods"
  default     = "ip-range-pods"
}

variable "ip_range_services_name" {
  description = "The secondary ip range to use for services"
  default     = "ip-range-services"
}

variable "cluster_name" {
    type        = string
    description = "The name of the cluster."
}

variable "node_pool_name" {
    description = "The environment for the GKE cluster"
    type = string
}

variable "network" {
    type = string
    description = "The VPC network created to host the cluster in"
}

variable "subnetwork" {
    type = string
    description = "The subnetwork created to host the cluster in"
}


provider "google" {
  credentials = "${file(var.credentials)}"
  project     = var.project_id
  region      = var.region
  zone        = var.zones[0]

  scopes = [
    # Default scopes
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
    "https://www.googleapis.com/auth/devstorage.full_control",

    # Required for google_client_openid_userinfo
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}

provider "google-beta" {
  credentials = "${file(var.credentials)}"
  project     = var.project_id
  region      = var.region
  zone        = var.zones[0]

  scopes = [
    # Default scopes
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
    "https://www.googleapis.com/auth/devstorage.full_control",

    # Required for google_client_openid_userinfo
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}


provider "kubernetes" {
  host                   = "https://${module.k8s.kubernetes_endpoint}"
  token                  = module.k8s.client_token
  cluster_ca_certificate = base64decode(module.k8s.ca_certificate)

#  config_path            = "~/.kube/config"

#host = "https://34.89.240.240"
#token = "ya29.c.b0AXv0zTMXLDle7fcF_fUfS-vQ63pQ5rkC0WWhiPWbhGf6oDX0Nr-JLVFp7OzvfAF_p-vRR_1tIm9-nlckvOkt27EVeQOGU7CZb9V6uB4VXVsZ96fUclRWFDlFQ8w3W6s2Gxo2s_AQ1IY8gOIzSfNieFjDeMI2ZL93uQrSr33bvaTEAfX2y2eNSOyOgDwHWeBxVFeb_5LZzVWU81pq_91r0R9HU2fth3M_J68zZjd9"
#cluster_ca_certificate = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUVMRENDQXBTZ0F3SUJBZ0lRUHQxUHduMWZTWTlveUJsOUVXcU1GREFOQmdrcWhraUc5dzBCQVFzRkFEQXYKTVMwd0t3WURWUVFERXlSaE9XSmlOakZtTWkwM09EQTVMVFJpWXpZdFlXTXpNaTFpTnpnNU1XVTNaVGMzT0dFdwpJQmNOTWpJd01UQTRNRFUxTURNeldoZ1BNakExTWpBeE1ERXdOalV3TXpOYU1DOHhMVEFyQmdOVkJBTVRKR0U1ClltSTJNV1l5TFRjNE1Ea3ROR0pqTmkxaFl6TXlMV0kzT0RreFpUZGxOemM0WVRDQ0FhSXdEUVlKS29aSWh2Y04KQVFFQkJRQURnZ0dQQURDQ0FZb0NnZ0dCQUswL2hic1czRlhCSHR3cXZQY0ZiT3lvWjlRcnR6ZlV6ZE1DL1FaQwpqSCtRUW5TdnJkWXFQRVlia3BUT1AralRDV2NqVThFM3BWZ0pPUzJRRFY0cDA0MFdEc09RTExManlCYUwvcWZmCkdmazIrUEZUY1hOMGluTy9ZS0hMZW4wWVdydUlSWjV0LzB2YVJ5dG1kSXN2R0ZHOVV6RWNabkxjT2FyZUgvYnoKYnJCNkRJVmxBQTdPbjMwU0tTWlFreWpIWUhhY05aamRVeE9rMGh3YzFrZXplNGJiTzJwQ0dCOFIvak13YTcwQwp3UTVIZ2JpOXZZVkthcXNudzNoWW5CMWJxUEdKRGFEd3JubG85emVReXJpTU9OV0lBQ1kyRHVDV3F6dXIyRkJNCnpEY01NV3k5L0V6Y1lFbXAxS2hoeDF2dklzYytZZHpGcUQ3MTJGVUExNTdUY1dSQVdNb1BSQjd0MjR4LzMxRjQKNHhza0xZRkpDVml4bGllRU9rS0IrRGFrSFRuS3BUWmlBZUVYdkNUUVR6enU3Nmg4YmxHZkpvOEtEeldBMitodwpZRHpOWCtHYTdvcER1RmpCdTBDWkhpTlNVUVRITFhUZ21lZkhwWm5zZzJhRGk0T3BOZERGL0xkaTQ2S3JNTi9WCmgrRTZoMlZZUnFMVUNKRXJBb2ttSTY3ZXl3SURBUUFCbzBJd1FEQU9CZ05WSFE4QkFmOEVCQU1DQWdRd0R3WUQKVlIwVEFRSC9CQVV3QXdFQi96QWRCZ05WSFE0RUZnUVVaT0tMS1Ntd3I5T2ZxSThSUVRZUnllSkxlakV3RFFZSgpLb1pJaHZjTkFRRUxCUUFEZ2dHQkFHRG84UzFQenloN2M5QXM0NmVBNGxqYXhVejN1V1o1eGRSalp6RE1wU01oCjYySmg2YVphdU9KSlQyOEdtVnRFTW5rcFg0OHVkblplV3FmUUQwTmQ3ZGpqNnZzdXhBdGdnWkF4Mkpic0Q1QTQKTTdiZ0xJYk4rbk53a2dVbG00cUVRL0JyNXpiYlhveUpOTTM1SlVucW9vQ0JWaThSZXZjMFAwZUgxSkUzczI2bgpwTVhoNi9uTjJqYUxGRGFqcWpidW5HWTltZjNYTGxjU1RUbzh0d0NTMWdkU2JIc2hkc1hYcXVRbTVaZjAybjVyCndLaS9wYzZodHJyYXc4UG5OSTVFdkpERU5JQUdDWmlYUTIwTGpiS3hSalh4SDBkU0JTWGs3Vjl6L2U5Y2ZLbmIKai8xWFhMNGpxWHdTYjFkcEVkUXZtZ1VmZXJWZTNNZFI1ckxkdDM0OGZFdDUrNkZ6ME5VTG9BU0ZiV1pqWmhFYgpyS3QvcnJzMHU3ZXR4VFFXQVlLWEtJWnZ5UXRDMFBlU3JVNExSZzI5UmFpcCtDdERORytGSGRqOEhrdm11dXgrCnlCWDlvV1lhUUU4OVdiUUwrTldBNGEvdjgvTzJ6WnRsdVpxWVpnVDRtZ1E2QXBkY2hqOGVVblg4aHZZRUMrWisKNU9DWVZ0c2hIcml1NlkzblZ3M1JyZz09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K"

}

provider helm {
  kubernetes {
#    config_path            = "~/.kube/config"
    host                   = "https://${module.k8s.kubernetes_endpoint}"
    token                  = module.k8s.client_token
    cluster_ca_certificate = base64decode(module.k8s.ca_certificate)

#host = "https://34.89.240.240"
#token = "ya29.c.b0AXv0zTMXLDle7fcF_fUfS-vQ63pQ5rkC0WWhiPWbhGf6oDX0Nr-JLVFp7OzvfAF_p-vRR_1tIm9-nlckvOkt27EVeQOGU7CZb9V6uB4VXVsZ96fUclRWFDlFQ8w3W6s2Gxo2s_AQ1IY8gOIzSfNieFjDeMI2ZL93uQrSr33bvaTEAfX2y2eNSOyOgDwHWeBxVFeb_5LZzVWU81pq_91r0R9HU2fth3M_J68zZjd9"
#cluster_ca_certificate = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUVMRENDQXBTZ0F3SUJBZ0lRUHQxUHduMWZTWTlveUJsOUVXcU1GREFOQmdrcWhraUc5dzBCQVFzRkFEQXYKTVMwd0t3WURWUVFERXlSaE9XSmlOakZtTWkwM09EQTVMVFJpWXpZdFlXTXpNaTFpTnpnNU1XVTNaVGMzT0dFdwpJQmNOTWpJd01UQTRNRFUxTURNeldoZ1BNakExTWpBeE1ERXdOalV3TXpOYU1DOHhMVEFyQmdOVkJBTVRKR0U1ClltSTJNV1l5TFRjNE1Ea3ROR0pqTmkxaFl6TXlMV0kzT0RreFpUZGxOemM0WVRDQ0FhSXdEUVlKS29aSWh2Y04KQVFFQkJRQURnZ0dQQURDQ0FZb0NnZ0dCQUswL2hic1czRlhCSHR3cXZQY0ZiT3lvWjlRcnR6ZlV6ZE1DL1FaQwpqSCtRUW5TdnJkWXFQRVlia3BUT1AralRDV2NqVThFM3BWZ0pPUzJRRFY0cDA0MFdEc09RTExManlCYUwvcWZmCkdmazIrUEZUY1hOMGluTy9ZS0hMZW4wWVdydUlSWjV0LzB2YVJ5dG1kSXN2R0ZHOVV6RWNabkxjT2FyZUgvYnoKYnJCNkRJVmxBQTdPbjMwU0tTWlFreWpIWUhhY05aamRVeE9rMGh3YzFrZXplNGJiTzJwQ0dCOFIvak13YTcwQwp3UTVIZ2JpOXZZVkthcXNudzNoWW5CMWJxUEdKRGFEd3JubG85emVReXJpTU9OV0lBQ1kyRHVDV3F6dXIyRkJNCnpEY01NV3k5L0V6Y1lFbXAxS2hoeDF2dklzYytZZHpGcUQ3MTJGVUExNTdUY1dSQVdNb1BSQjd0MjR4LzMxRjQKNHhza0xZRkpDVml4bGllRU9rS0IrRGFrSFRuS3BUWmlBZUVYdkNUUVR6enU3Nmg4YmxHZkpvOEtEeldBMitodwpZRHpOWCtHYTdvcER1RmpCdTBDWkhpTlNVUVRITFhUZ21lZkhwWm5zZzJhRGk0T3BOZERGL0xkaTQ2S3JNTi9WCmgrRTZoMlZZUnFMVUNKRXJBb2ttSTY3ZXl3SURBUUFCbzBJd1FEQU9CZ05WSFE4QkFmOEVCQU1DQWdRd0R3WUQKVlIwVEFRSC9CQVV3QXdFQi96QWRCZ05WSFE0RUZnUVVaT0tMS1Ntd3I5T2ZxSThSUVRZUnllSkxlakV3RFFZSgpLb1pJaHZjTkFRRUxCUUFEZ2dHQkFHRG84UzFQenloN2M5QXM0NmVBNGxqYXhVejN1V1o1eGRSalp6RE1wU01oCjYySmg2YVphdU9KSlQyOEdtVnRFTW5rcFg0OHVkblplV3FmUUQwTmQ3ZGpqNnZzdXhBdGdnWkF4Mkpic0Q1QTQKTTdiZ0xJYk4rbk53a2dVbG00cUVRL0JyNXpiYlhveUpOTTM1SlVucW9vQ0JWaThSZXZjMFAwZUgxSkUzczI2bgpwTVhoNi9uTjJqYUxGRGFqcWpidW5HWTltZjNYTGxjU1RUbzh0d0NTMWdkU2JIc2hkc1hYcXVRbTVaZjAybjVyCndLaS9wYzZodHJyYXc4UG5OSTVFdkpERU5JQUdDWmlYUTIwTGpiS3hSalh4SDBkU0JTWGs3Vjl6L2U5Y2ZLbmIKai8xWFhMNGpxWHdTYjFkcEVkUXZtZ1VmZXJWZTNNZFI1ckxkdDM0OGZFdDUrNkZ6ME5VTG9BU0ZiV1pqWmhFYgpyS3QvcnJzMHU3ZXR4VFFXQVlLWEtJWnZ5UXRDMFBlU3JVNExSZzI5UmFpcCtDdERORytGSGRqOEhrdm11dXgrCnlCWDlvV1lhUUU4OVdiUUwrTldBNGEvdjgvTzJ6WnRsdVpxWVpnVDRtZ1E2QXBkY2hqOGVVblg4aHZZRUMrWisKNU9DWVZ0c2hIcml1NlkzblZ3M1JyZz09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K"

  }
}

provider "null" {
  # Configuration options
}
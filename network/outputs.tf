output "network_name" {
  value       = module.vpc.network_name
  description = "The name of the VPC being created"
}

output "network_self_link" {
  value       = module.vpc.network_self_link
  description = "The URI of the VPC being created"
}

output "subnets_regions" {
  value       = module.vpc.subnets_regions
  description = "The region where subnets will be created"
}

output "route_names" {
  value       = module.vpc.route_names
  description = "The routes associated with this VPC"
}
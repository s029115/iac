module "vpc" {
    source       = "terraform-google-modules/network/google"
    project_id   = var.project_id
    network_name = var.network
    routing_mode = "GLOBAL"

    subnets = [
        {
            subnet_name   = "${var.subnetwork}"
            subnet_ip     = "10.10.0.0/16"
            subnet_region = var.region
        },
    ]

    secondary_ranges = {
        "${var.subnetwork}" = [
            {
                range_name    = var.ip_range_pods_name
                ip_cidr_range = "172.18.0.0/17"
            },
            {
                range_name    = var.ip_range_services_name
                ip_cidr_range = "172.18.128.0/17"
            },
        ]
    }

    # routes = [
    #     {
    #         name                   = "egress-internet"
    #         description            = "route through IGW to access internet"
    #         destination_range      = "0.0.0.0/0"
    #         tags                   = "egress-inet"
    #         next_hop_internet      = "true"
    #     },
    # ]

    firewall_rules = [
        {
            name      = "allow-ssh-ingress"
            direction = "INGRESS"
            ranges    = ["0.0.0.0/0"]
            allow = [{
                protocol = "tcp"
                ports    = ["22"]
            }]
            log_config = {
                metadata = "INCLUDE_ALL_METADATA"
            }
        },
        {
            name      = "allow-icmp"
            direction = "INGRESS"
            ranges    = ["0.0.0.0/0"]
            allow = [{
                protocol = "icmp"
                ports    = null
            }]
            log_config = {
                metadata = "INCLUDE_ALL_METADATA"
            }
        },
        # {
        #     name      = "deny-udp-egress"
        #     direction = "INGRESS"
        #     ranges    = ["0.0.0.0/0"]
        #     deny = [{
        #         protocol = "udp"
        #         ports    = null
        #     }]
        # },
    ]

}
terraform {
  backend "gcs" {
    credentials = "cred/terraform-gcp.json"
    bucket      = "fps029115"
    prefix      = "terraform/state"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.87.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.5.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.7.1"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.4.1"
    }
    null = {
      source = "hashicorp/null"
      version = "3.1.0"
    }
  }

  required_version = "= 1.1.2"
}

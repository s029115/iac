/* Базовые переменные */
variable "credentials" {
  type        = string
  description = "Location of the credential keyfile."
  default = "./cred/terraform-gcp.json"
}

variable "project_id" {
  type        = string
  description = "The project ID to create the cluster."
}

variable "region" {
  type        = string
  description = "The region to create the cluster."
  default = "europe-west3"
}

variable "zones" {
  type        = list(string)
  description = "The zones to create the cluster."
  default = ["europe-west3-b"]
}

/* Переменные для Gitlab-runner */

variable "gitlab_runner_registration_token" {
  description = "gitlab_runner_registration_token"
  type = string
}

/* Переменные для SQL Instance */

variable "sql_user_password" {
  type = string
}

